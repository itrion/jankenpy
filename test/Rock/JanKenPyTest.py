import unittest

from test.JanKenPy import JanKenPy
from test.Rock.Paper import Paper
from test.Rock.Rock import Rock
from test.Scissors import Scissors


class PlayerTest(unittest.TestCase):
    def test_drawWhenTwoScissors(self):
        game = JanKenPy()
        result = game.clash(Scissors(), Scissors())
        self.assertEqual("draw", result.description)

    def test_rockWinsToPaper(self):
        game = JanKenPy()
        result = game.clash(Rock(), Scissors())
        self.assertEqual("rock wins", result.description)

    def test_paperWinsToRock(self):
        game = JanKenPy()
        result = game.clash(Paper(), Rock())
        self.assertEqual("paper wins", result.description)
